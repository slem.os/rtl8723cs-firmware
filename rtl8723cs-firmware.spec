Name:    rtl8723cs-firmware
Summary: Proprietary realtek bluetooth firmware
Version: 1.0
Release: 2%{?dist}

License: Redistributable, no modification permitted
URL:     https://github.com/anarsoul/rtl8723bt-firmware
Source0: rtl8723cs_xx_config-pinephone.bin
Source1: rtl8723cs_xx_fw.bin
Source2: anx7688-fw.bin

%description
Bluetooth firmware for the pinephone.

%prep
echo No prep neccesary, proprietary.

%build
echo No build neccesary, proprietary.

%install
mkdir -p ${RPM_BUILD_ROOT}/lib/firmware/rtl_bt/
cp %{SOURCE0} ${RPM_BUILD_ROOT}/lib/firmware/rtl_bt/
cp %{SOURCE1} ${RPM_BUILD_ROOT}/lib/firmware/rtl_bt/
cp %{SOURCE2} ${RPM_BUILD_ROOT}/lib/firmware/

%check
echo No way to check, proprietary.

%files
%dir /lib/firmware
/lib/firmware/anx7688-fw.bin
%dir /lib/firmware/rtl_bt
/lib/firmware/rtl_bt/*

%changelog
* Sat Jun 20 2020 Adrian Campos <adriancampos@teachelp.com> - 1.0-2
- First build 
